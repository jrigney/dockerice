#Dockerice

Variety of Dockerfiles

## todo
- learn vault for secrets
- learn approle for authentication

## vault

### running vault
docker run -p 8200:8200 vault

docker run --cap-add IPC_LOCK -e 'VAULT_DEV_ROOT_TOKEN_ID=myroot' -e 'VAULT_DEV_LISTEN_ADDRESS=0.0.0.0:1234' vault

### using the vault cli

docker exec -i -e VAULT_ADDR='http://0.0.0.0:8200' -t hopeful_goldberg vault status


## oauthProxy
port 8001 is exposed for the proxy

```
cd oauthProxy
docker build -t oauth-proxy .
```

### run oauthProxy in the background

notice the -d in the run command

```
docker run -d --name oauth-proxy-sitb -p 8001:8001 oauth-proxy --consumer-key <key> --consumer-secret <secret>
```

### run oauthProxy in the foreground

```
docker run --name oauth-proxy-sitb -p 8001:8001 oauth-proxy --consumer-key <key> --consumer-secret <secret>
```

```
curl -x localhost:8001 http://host.name/path
```

## Mnemosyne Project 2.4.1

A docker image to run a mnemosyne sync server

http://mnemosyne-proj.org/

### todo
- create a data volume container to contain backup directories
- create a backup job for mnemosyne data
- create a setup script for the data container and mnemosyne

### mnemosyne config files
cp -r  ~/.local/share/mnemosyne backup/mnemosyneShare
cp -r  ~/.config/mnemosyne backup/mnemosyneConfig

### debug the container
docker run -p 8512:8512 -p 8513:8513 --rm -it mnemosyne /bin/bash

docker logs <container_name> to retrieve the logs

docker exec -ti <containerId> /bin/bash attach to a running container

### run the container
docker run --name mnemosyne -p 8512:8512  -p 8513:8513  mnemosyne



