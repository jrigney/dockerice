# rsync-docker

## build
docker build -t rsync .

## run

rsync files from a host mounted filesystem to a data container(dataContainer)
```
docker run --volumes-from dataContainer -v $(pwd):/backup rsync -zh /backup/Dockerfile /data
```

--volumes-from dataContainer is a docker data container
-v $(pwd):/backup mounts the current directory as /backup it the container


docker run --volumes-from dataContainer -v $(pwd):/backup rsync -zh /data/Dockerfile mark@172.16.1.40:/tmp


## data container

### find data container
docker ps -a | grep dataContainer

### inspect container
docker inspect dataContainer



